Description
===========
This is a base project in Vagrant to support Drupal 8 environment ready to test custom modules to be promoted to contribution. Vagrant is a way to have a similar working context, and it is perfect when you are working out of Acquia or Pantheon.

Requirements
------------
In order to run this project, you require to have installed

[Vagrant](https://www.vagrantup.com/)

[VirtualBox](https://www.virtualbox.org/)

Setup
-----
Clon this repo
```bash
$ git clone https://diegoluisr@bitbucket.org/diegoluisr/drupal-custom-modules.git
```
Open your terminal,change directory, and run next command
```bash
$ cd drupal-custom-modules
$ vagrant up
```
This command will execute all tasks related to set ready the environment. Then you could go into the virtualized machine using next command
```bash
$ vagrant ssh
```
In your host machine open this link in your favorite browser, use this URL to access administration panel
http://localhost:8282/user/login

> **Credentials**
> Username: admin
> 
> Password: admin
>

In another tab open this link, use this URL to access as a anonymous user.
http://127.0.0.1:8282/

Drupal creates separated cache for each domain


If you want to update "Exit Notifier" module configuration you need to access to http://localhost:8282/admin/config/user-interface/exit_notifier/settings


Tests (in progress)
-------------------

If you wanto to test manually the module please click "External test" menu option on http://127.0.0.1:8282/ You could see a modal window open with its configuration information

If you want to text through commandline please run next command on your guest machine
```bash
$ cd /vagrant/tests
$ sh run-tests.sh
```

If you want to test project via Drupal administration panel, You could access this link http://localhost:8282/admin/config/development/testing , here you need to find "exit_notifier" group and check those tests that you want to run
