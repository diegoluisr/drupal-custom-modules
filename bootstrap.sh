#!/usr/bin/env bash

#Variables
DBNAME=cmodules_db
DBUSERNM=root
DBPASSWD=toor
PHPCONFDIR=/etc/php/7.1/apache2
DRUPALV=8.4.3

locale-gen es_ES.UTF-8 > /dev/null 2>&1
dpkg-reconfigure locales > /dev/null 2>&1
export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8

echo -e "\n--- Ready to install... ---\n"

echo -e "\n--- Updating packages list ---\n"
apt-get -qq update

echo -e "\n--- Installing base packages [1/3] (CURL)  ---\n"
apt-get -y install curl > /dev/null 2>&1
echo -e "\n--- Installing base packages [2/3] (Build-Essential)  ---\n"
apt-get -y install build-essential > /dev/null 2>&1

echo -e "\n--- Installing base packages [3/3] (GIT)  ---\n"
apt-get -y install git > /dev/null 2>&1

echo -e "\n--- Updating packages list - Again ---\n"
apt-get -qq update

echo -e "\n--- Install MySQL specific packages and settings ---\n"
echo "mysql-server mysql-server/root_password password $DBPASSWD" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password $DBPASSWD" | debconf-set-selections
apt-get -y install mysql-server > /dev/null 2>&1

echo -e "\n--- Creating Drupal database schema ---\n"
mysql -u $DBUSERNM -p$DBPASSWD -e "CREATE DATABASE $DBNAME DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci"

#https://www.digitalocean.com/community/tutorials/how-to-upgrade-to-php-7-on-ubuntu-14-04
echo -e "\n--- Adding suport to PHP 7.1 on Ubuntu 14.04 ---\n"
apt-get install -y language-pack-en-base > /dev/null 2>&1
LC_ALL=en_US.UTF-8 add-apt-repository ppa:ondrej/php -y > /dev/null 2>&1
apt-get -qq update

echo -e "\n--- Installing packages [1/10] (Apache) ---\n"
apt-get install -y apache2 > /dev/null 2>&1
echo -e "\n--- Installing packages [2/10] (Unzip) ---\n"
apt-get install -y unzip > /dev/null 2>&1
echo -e "\n--- Installing packages [3/10] (PHP) ---\n"
apt-get -y install php7.1 > /dev/null 2>&1
echo -e "\n--- Installing packages [4/10] (PHP - CURL) ---\n"
apt-get -y install php7.1-curl > /dev/null 2>&1
echo -e "\n--- Installing packages [5/10] (PHP - GD) ---\n"
apt-get -y install php7.1-gd > /dev/null 2>&1
echo -e "\n--- Installing packages [6/10] (PHP - MCRYPT) ---\n"
apt-get -y install php7.1-mcrypt > /dev/null 2>&1
echo -e "\n--- Installing packages [7/10] (PHP - MySQL) ---\n"
apt-get -y install php7.1-mysql > /dev/null 2>&1
echo -e "\n--- Installing packages [8/10] (PHP - APCU) ---\n"
apt-get -y install php7.1-apcu > /dev/null 2>&1
echo -e "\n--- Installing packages [9/10] (PHP - XML) ---\n"
apt-get -y install php7.1-xml > /dev/null 2>&1
echo -e "\n--- Installing packages [10/10] (PHP - MBString) ---\n"
apt-get -y install php7.1-mbstring > /dev/null 2>&1

echo -e "\n--- Allowing Apache override to all ---\n"
sed -i "s/AllowOverride None/AllowOverride All/g" /etc/apache2/apache2.conf

echo -e "Changing apache user and group to 'vagrant'"
sed -i 's/User ${APACHE_RUN_USER}/User vagrant/g' /etc/apache2/apache2.conf
sed -i 's/Group ${APACHE_RUN_GROUP}/Group vagrant/g' /etc/apache2/apache2.conf

echo -e "\n--- Enabling Apache Mod Rewrite ---\n"
a2enmod rewrite

echo -e "\n--- Restarting Apache ---\n"
service apache2 restart > /dev/null 2>&1

echo -e "\n--- Creating work folders ---\n"
if ! [ -d /vagrant/www/html ]; then
  mkdir -p /vagrant/www/html
fi

echo -e "\n--- Symlink from work folder to webserver root ---\n"
if ! [ -L /var/www ]; then
  rm -rf /var/www
  ln -fs /vagrant/www /var/www
fi

echo -e "\n--- Turning on PHP errors ---\n"
sed -i "s/error_reporting = .*/error_reporting = E_ALL/" $PHPCONFDIR/php.ini
sed -i "s/display_errors = .*/display_errors = On/" $PHPCONFDIR/php.ini
sed -i "s/memory_limit = 128M/memory_limit = 1024M/" $PHPCONFDIR/php.ini

echo -e "\n--- Installing Composer (PHP package management) ---\n"
curl --silent https://getcomposer.org/installer | php > /dev/null 2>&1
chmod +x composer.phar
mv composer.phar /usr/local/bin/composer

echo -e "\n--- Installing Composer dev dependencies ---\n"
composer install --dev > /dev/null 2>&1

echo -e "\n--- Installing Drush (Drupal - Commandline tool) ---\n"
wget -O /usr/local/bin/drush https://github.com/drush-ops/drush/releases/download/8.1.15/drush.phar > /dev/null 2>&1
chmod +x /usr/local/bin/drush

if ! [ -f /vagrant/www/html/index.php ]; then
  echo -e "\n--- Downloading Drupal ---\n"
  cd /vagrant/www/html
  wget http://ftp.drupal.org/files/projects/drupal-$DRUPALV.tar.gz > /dev/null 2>&1
  tar -xvzf drupal-$DRUPALV.tar.gz > /dev/null 2>&1
  rm drupal-$DRUPALV.tar.gz
  cp -r drupal-$DRUPALV/. ./
  rm -r drupal-$DRUPALV
  if ! [ -d /var/www/html/sites/default/files ]; then
    mkdir -p /var/www/html/sites/default/files
  fi
fi

if [ -d /var/www/html/sites/default/files ]; then
  chmod -R 0770 /var/www/html/sites/default/files
fi

if [ -d /var/www/html/modules/custom/exit_notifier ]; then
  rm -Rf /var/www/html/modules/custom/exit_notifier
fi

if [ -d /var/www/html/modules/custom ]; then
  cd /var/www/html/modules/custom
  git rm exit_notifier
  git submodule add https://git.drupal.org/project/exit_notifier.git exit_notifier
fi

if [ -f /vagrant/database/backup.last.sql ]; then
  echo -e "\n--- Restoring database last version  ---\n"
  cd /vagrant/database
  sh local.restore.sh
fi
