#!/usr/bin/env bash

FILENAME=backup.$(date +%Y%m%d_%H%M%S).sql
CURDIR=$(pwd)

cd /var/www/html

drush cr

drush sql-dump --result-file=$CURDIR/$FILENAME

if [ -f $CURDIR/backup.last.sql ]; then
  rm $CURDIR/backup.last.sql
fi

cp $CURDIR/$FILENAME $CURDIR/backup.last.sql
echo "--- done! ---\n"