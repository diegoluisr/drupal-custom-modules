#!/usr/bin/env bash

CURDIR="$(pwd)"

echo "${CURDIR}"

if [ -f "${CURDIR}/backup.last.sql" ]; then
  cd /var/www/html
  drush sql-cli < "${CURDIR}/backup.last.sql"
fi
echo "--- done! ---\n"